# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
source scl_source enable gcc-toolset-10
export OMP_NUM_THREADS=1
export OMP_PROC_BIND=close
export OMP_PLACES=cores
export PATH=/opt/rh/gcc-toolset-10/root/bin/:$PATH
#export LD_LIBRARY_PATH=/opt/hesm/hesm_2020_07_29/lib64:/opt/hesm/hesm_2020_07_29/lib:$LD_LIBRARY_PATH
export TRILINOS_PATH=/models/rsm/lib_env_20230215/trilinos
FC=gfortran

export PATH=/home/glagerwall/lib_env/libxml++/include/libxml++-5.0/:/usr/include/libxml2/libxml/:/home/glagerwall/lib_env/libxml++/lib/:$PATH

#libnetcdf for wbbud and other tools
#export PATH=/opt/hesm/hesm_2020_06_23/lib/libnetcdf.so.18:$PATH
#alias libnetcdf.so.18=/usr/lib64/openmpi/lib/libnetcdf.so.15 #/opt/hesm/hesm_2020_06_23/lib/libnetcdf.so.18

### Emacs Alias for loading client
#alias emacss='snap run emacs&'
alias emacscw="/usr/local/bin/emacsclient -c -n -a \"\""
alias emacsc="/usr/local/bin/emacsclient -c -nw -a \"\""
alias emacs="/usr/local/bin/emacs -nw"
alias emacsw="/usr/local/bin/emacs"

### Ifort requirements
alias ifort="/opt/intel/oneapi/compiler/2023.0.0/linux/bin/intel64/ifort"
source /opt/intel/oneapi/compiler/2023.0.0/env/vars.sh

### HEC-DSS Vue
alias hecdssvue="/opt/hec-dssvue-3.2.9/hec-dssvue.sh"

### For ease of navigating
export RSM=/models/rsm
export RSMPP=/models/rsm/postproc
export RSMPPS=/models/rsm/postproc/scripts
export RSMSIM=/models/rsm/simulations

### Custom script to select correct version of rsm and update links
alias rsm="/models/rsm/postproc/scripts/rsmRunVersion.sh"

### Bash settings for inherited permissions
### used setfacl on directory for group to achieve desired permissions
### By default files dont inherit executability, only when compiled
#umask 003

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/anaconda/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/anaconda/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/opt/anaconda/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/opt/anaconda/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup


# The base environment is not activated by default
conda config --set auto_activate_base false
# <<< conda initialize <<<

# Base16 Shell
# BASE16_SHELL="$HOME/.config/base16-shell/"
# [ -n "$PS1" ] && \
#     [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
#         source "$BASE16_SHELL/profile_helper.sh"
        
# base16_zenburn

### TMUX
#start tmux in 256 color mode
alias tmux="/usr/bin/tmux -2"
. "$HOME/.cargo/env"
## From https://unix.stackexchange.com/questions/43601/how-can-i-set-my-default-shell-to-start-up-tmux
# if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
#   exec "/usr/bin/tmux -2"
# fi

### Run wttr.in at init
#alias weather="curl wttr.in/miami?0?m"

### Run neofetch at initialization
# looks like it runs automatically anyway
# from: https://forum.endeavouros.com/t/tip-a-better-way-to-add-neofetch-to-your-bashrc/15116
#neofetch
#neofetch --ascii "$(fortune -es | cowsay -W 30 -f $(find /usr/share/cowsay -type f | shuf -n 1))"
#neofetch --ascii "$(fortune -es | fmt -80 -s | $(shuf -n 1 -e cowsay cowthink) -$(shuf -n 1 -e b d g p s t w y) -f $(shuf -n 1 -e $(cowsay -l | tail -n +2)) -n)"
#cat .nf 2> /dev/null
#setsid neofetch >| .nf

### Run fortune, cowsay, lolcrab
#alias fortcol="fortune -es | fmt -80 -s | $(shuf -n 1 -e cowsay cowthink) -$(shuf -n 1 -e b d g p s t w y) -f $(shuf -n 1 -e $(cowsay -l | tail -n +2)) -n | lolcrab"

## random run wttr, neofetch, fortcol
#$(shuf -e -n 1 "curl wttr.in/miami?0?m" neofetch)
#$(shuf -e -n 1 "curl wttr.in/miami?0?m" neofetch <(fortune -es | lolcrab))
#$(shuf -e -n 1 "fortune -es | lolcrab")
#neofetch --ascii "$(fortcol)"

inscr=$(shuf -e -n 1 case1 case2 case3)
case $inscr in
    case1) fortune -es | fmt -80 -s | $(shuf -n 1 -e cowsay cowthink) -$(shuf -n 1 -e b d g p s t w y) -f $(shuf -n 1 -e $(cowsay -l | tail -n +2)) -n | lolcrab ;;
    case2) neofetch ;;
    case3) curl wttr.in/miami?0?m ;;
esac
#echo "hello world"

### Ranger vi terminal file browser
RANGER_LOAD_DEFAULT_RC=false

### terminal colour scheme
#export TERM=xterm-256color

###
